/**
 sllist: simple implementation of singly linked list data structure
 Author: Zhiliang Su (zsu2 [at] buffalo [dot] edu)
 Revised: Oct 31, 2012
*/
#ifndef __HEADER_SLLIST__
#define __HEADER_SLLIST__

#include <stdio.h>
#include <stdlib.h>

/**
 type _sllist_node, a node struct defined for this singly linked list
*/
typedef struct __sllist_node{
    void * data;
    struct __sllist_node * next;
} _sllist_node;

/**
 type sllist_entity, a structure defined to represent a singly linked list
*/
typedef struct _sllist_entity{
    int elem_count;
    _sllist_node nullhead;
    _sllist_node * tail;
} sllist_entity;

/** 
 func: sllist_init
  to initialize a sllist_entity (singly linked list)
    input:
        sllist_entity * : the pointer poiting to the entity needed to be inited
*/
void sllist_init(sllist_entity *);

/**
 func: sllist_append
  to append a pointer of the data to the end of the list
    input:
        sllist_entity *: the sllist object that needed to be OPed on
        void * : the void pointer pointing to the data address
    output:
        int :   0 indicates failure, otherwise success
*/
int sllist_append(sllist_entity *, void *);

/**
 func: sllist_pop
  to pop a data pointer stored in the first node of the list;
    input:
        sllist_entity *: the sllist object that needed to be OPed on
    output:
        void * : the void pointer pointing to the data address
*/
void * sllist_pop(sllist_entity *);

/**
 func: sllist_insert_after
  to insert a pointer (in a new node) of the data after the "pos"-th node
    input:
        sllist_entity *: the sllist object that needed to be OPed on
        int pos:    the number of nodes to be inserted after
        void * :    the void pointer pointing to the data address
    output:
        int :   0 indicates failure, otherwise success
*/
int sllist_insert_after(sllist_entity *, int pos, void *);

/**
 func: sllist_remove_after
  to remove a node that is after the "pos"-th node
    input:
        sllist_entity * : the sllist object that needed to be OPed on
        int pos:    the number of nodes to be inserted after
    output:
        void * : the popped out data
*/
void * sllist_remove_after(sllist_entity *, int pos);

/**
 func: sllist_destroy
  to destroy a sllist entity
    input:
        sllist_entity * : the sllist object that needed to be OPed on
*/
void sllist_destroy(sllist_entity *);
#endif
