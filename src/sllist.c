#include "sllist.h"

void sllist_init(sllist_entity * e){
    e->elem_count = 0;
    (e->nullhead).next = NULL;
    e->tail = NULL;
}

int sllist_append(sllist_entity * e, void * _idata){
    _sllist_node * _new_node = malloc(sizeof(_sllist_node));
    if (_new_node == NULL){return 0;}
    _new_node->data = _idata;
    _new_node->next = NULL;
    if (e->elem_count == 0){
       (e->nullhead).next = _new_node;
       e->tail = _new_node;
    } else {
        (e->tail)->next = _new_node;
        e->tail = e->tail->next;
    }
    ++(e->elem_count);
    return 1;
}

void * sllist_pop(sllist_entity * e){
    if (e->elem_count == 0){return NULL;}
    void * _odata = ((e->nullhead).next)->data;
    _sllist_node * _to_be_rm_node = (e->nullhead).next;
    (e->nullhead).next = ((e->nullhead).next)->next;
    --(e->elem_count);
    free(_to_be_rm_node);
    return _odata;
}

int sllist_insert_after(sllist_entity * e, int pos, void * _idata){
    if (pos < 0) {return 0;}
    if (e->elem_count < pos){return 0;}
    _sllist_node * _pick = &(e->nullhead);
    _sllist_node * _new_node = malloc(sizeof(_sllist_node));
    int i;
    for (i=0; i<pos; ++i){_pick = _pick->next;}
    _new_node->data = _idata;
    _new_node->next = _pick->next;
    _pick->next = _new_node;
    ++(e->elem_count);
    return 1;
}

void * sllist_remove_after(sllist_entity * e, int pos){
    if (pos < 0) {return 0;}
    if (e->elem_count <= pos){return NULL;}
    _sllist_node * _pick = &(e->nullhead);
    int i;
    for (i=0; i<pos; ++i){_pick = _pick->next;}
    void * _tmp_data = (_pick->next)->data;
    _sllist_node * _to_be_rm_node = _pick->next;
    _pick->next = (_pick->next)->next;
    --(e->elem_count);
    free(_to_be_rm_node);
    return _tmp_data;
}

void sllist_destroy(sllist_entity * e){
    if (e->tail){free(e->tail);}
    free(e);
}
