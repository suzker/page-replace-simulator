#include "utility.h"

char arg_input_filepath[2000];
int arg_print_usage = -1;
int arg_available_frame = -1;
int arg_replace_policy = -1;

int RPLCY_FIFO = 1;
int RPLCY_LFU = 2;
int RPLCY_LRU_STACK = 3;
int RPLCY_LRU_CLOCK = 4;
int RPLCY_LRU_REF8 = 5;
int RPLCY_BELADY = 6;

int util_arg_parser(int argc, char * argv[]){
   init_arg();
   int i;
   int err = 0;
   for (i = 1; i < argc; ++i){
       if (err) {break;}
       if (argv[i][0] != '-'){
           err = 1;
           break;
       } else {
           switch (argv[i][1]){
               case 'h':
                    arg_print_usage = 1;
                    break;
               case 'f':
                    arg_available_frame = ++i < argc ? atoi(argv[i]) : 5;
                    err = i < argc ? 0 : 1;
                    break;
               case 'i':
                    if (++i < argc){
                        strcpy(arg_root_folder, argv[i]);
                    } else {
                        err = 1;
                    }
                    break;
               case 's':
                    if (++i < argc){
                        if(strcmp(argv[i], "FIFO") == 0){
                            arg_replace_policy = RPLCY_FIFO;
                            break;
                        }
                        if(strcno(argv[i], "LFU") == 0){
                            arg_replace_policy = RPLCY_LFU;
                            break;
                        }
                        if(strcmp(argv[i], "LRU_STACK") == 0){
                            arg_replace_policy = RPLCY_LRU_STACK;
                            break;
                        }
                        if(strcmp(argv[i], "LRU_CLOCK") == 0){
                            arg_replace_policy = RPLCY_LRU_CLOCK
                            break;
                        }
                        if(strcmp(argv[i], "LRU_REF8") == 0){
                            arg_replace_policy = RPLCY_LRU_REF8;
                            break;
                        }
                        if(strcmp(argv[i], "OPTIMAL") == 0){
                            arg_replace_policy = RPLCY_BELADY;
                            break;
                        }
                    } else {
                        err = 1;
                    }
                    break;
               default:
                    err = 1;
                    break;
               }
          }
   }
   if (err) {
       printf ("Argument Error! \n");
       return 0;
   }
}

void util_init_arg(){
    arg_print_usage = 0;
    arg_available_frame = 5;
    arg_replace_policy = RPLCY_FIFO;
    strcpy(arg_input_filepath, "N/A");
}
