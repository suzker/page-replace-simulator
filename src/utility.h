#ifndef __HEADER_UTILITY__
#define __HEADER_UTILITY__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int arg_print_usage;
extern int arg_available_frame;
extern int arg_replace_policy;
extern char arg_input_filepath[];

/**
 func: util_arg_parser
  to parse the input arguments
  input: 
    int argc: number of args
    char ** argv: separated argment string values
  output:
    int rtr: 0 for unsuccessful otherwise success
*/
int util_arg_parser(int argc, char * argv[]);

/**
 func: util_init_arg
  to initialize arguments
*/
void util_init_arg();
#endif
